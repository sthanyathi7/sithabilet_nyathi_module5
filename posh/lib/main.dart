import 'package:flutter/material.dart';
import 'package:posh/screens/addSession.dart';
import './screens/screen_index.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyCG-HALhSe_HJOSH5X2p4eHlYPMvlbRF-E",
          authDomain: "module5-app.firebaseapp.com",
          projectId: "module5-app",
          storageBucket: "module5-app.appspot.com",
          messagingSenderId: "97075438169",
          appId: "1:97075438169:web:3b13192a001c73837f8748"));
  runApp(const MyApp());
}

@override
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        // to configure the routes
        title: 'Promotion Sessions',
        theme: ThemeData(
          brightness: Brightness.light,
          primarySwatch: Colors.lime,
          colorScheme: const ColorScheme(
            brightness: Brightness.light,
            primary: Colors.lime,
            onPrimary: Colors.black,
            secondary: Colors.lime,
            onSecondary: Colors.black,
            background: Colors.grey,
            onBackground: Colors.white,
            surface: Colors.red,
            onSurface: Colors.black,
            error: Colors.red,
            onError: Colors.amber,
          ),
          scaffoldBackgroundColor: const Color.fromARGB(189, 189, 189, 189),
          fontFamily: 'Georgia',
          textTheme: const TextTheme(
            bodyText1: TextStyle(
                fontSize: 12,
                fontFamily: 'Hind',
                fontWeight: FontWeight.w600,
                color: Colors.black),
          ),
        ),
        debugShowCheckedModeBanner: false,
        routes: {
          '/': (context) => const FirstScreen(),
          '/s': (context) => const AddSession(),
          '/h': (context) => const HomePage(),
          '/d': (context) => const Dashboard(),
          '/r': (context) => const Registration(),
          '/l': (context) => const Login(),
          '/e': (context) => const Edit(),
          '/p': (context) => const PromotionsPage(),
        });
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[AddSession()]),
      ),
    );
  }
}
