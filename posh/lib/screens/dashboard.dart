import 'package:posh/screens/addSession.dart';
import 'package:posh/screens/screen_index.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
        child: GridView.count(
          crossAxisCount: 2,
          padding: const EdgeInsets.all(3.0),
          children: <Widget>[
            makeDashboardItem(
                "Posh",
                const Image(
                  image: AssetImage('images/saloon08.png'),
                ),
                context),
            makeDashboardItem(
                "Eminence",
                const Image(
                  image: AssetImage('images/saloon07.png'),
                ),
                context),
            makeDashboardItem(
                "MakeUp",
                const Image(
                  image: AssetImage('images/saloon03.png'),
                ),
                context),
            makeDashboardItem(
                "style",
                const Image(
                  image: AssetImage('images/saloon08.png'),
                ),
                context),
          ],
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.lime,
              ),
              child: Text(
                'Posh Offerings',
                style: TextStyle(
                    fontSize: 30, fontFamily: 'Raleway', color: Colors.black87),
              ),
            ),
            ListTile(
              title: Row(
                children: const [
                  Icon(Icons.menu_book_rounded),
                  Text('Book A Session'),
                ],
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const AddSession()),
                );
              },
            ),
            ListTile(
              title: Row(
                children: const [
                  Icon(Icons.sell),
                  Text('Promotions'),
                ],
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const PromotionsPage()),
                );
              },
            ),
            ListTile(
              title: Row(
                children: const [
                  Icon(Icons.now_widgets_outlined),
                  Text('Maintenance'),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Row(
                children: const [
                  Icon(Icons.logout_outlined),
                  Text('Logout'),
                ],
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const FirstScreen()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
