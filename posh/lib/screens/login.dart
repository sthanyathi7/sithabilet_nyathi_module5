import 'package:posh/screens/screen_index.dart';
import 'package:flutter/material.dart';
import './screen_index.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Login"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(28.0),
          child: Column(
            children: [
              const Text(
                'Login',
                style: TextStyle(fontSize: 28, height: 2),
              ),
              TextFormField(
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter username',
                  hintText: '@email',
                ),
              ),
              TextFormField(
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter password',
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(5, 25, 50, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                      style: ElevatedButton.styleFrom(
                        // Foreground color
                        onPrimary:
                            Theme.of(context).colorScheme.onSecondaryContainer,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 20),

                        // Background color
                        primary:
                            Theme.of(context).colorScheme.secondaryContainer,
                      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                        'Cancel',
                      ),
                    ),
                    TextButton(
                      style: ElevatedButton.styleFrom(
                        // Foreground color
                        onPrimary:
                            Theme.of(context).colorScheme.onSecondaryContainer,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 20),

                        // Background color
                        primary:
                            Theme.of(context).colorScheme.secondaryContainer,
                      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Dashboard()),
                        );
                      },
                      child: const Text(
                        'Login',
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
