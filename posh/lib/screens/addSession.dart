// ignore_for_file: unnecessary_new

import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
//
import 'package:flutter/material.dart';
import 'package:posh/screens/sessionsList.dart';

class AddSession extends StatefulWidget {
  const AddSession({super.key});

  @override
  State<AddSession> createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController surnameController = TextEditingController();
    TextEditingController provinceController = TextEditingController();

    Future _addSessions() {
      final name = nameController.text;
      final surname = surnameController.text;
      final province = provinceController.text;

      final ref = FirebaseFirestore.instance.collection("sessions").doc();

      return ref
          .set({
            "name": name,
            "surname": surname,
            "province": province,
            "doc_id": ref.id
          })
          .then((value) => log("Promotion added!"))
          .catchError((onError) => log(onError));
    }

    return new Scaffold(
        appBar: AppBar(
          title: const Text(
            "Promotion Sessions",
            textAlign: TextAlign.center,
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              children: [
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 8),
                        child: TextField(
                            controller: nameController,
                            decoration: const InputDecoration(
                                border: UnderlineInputBorder(),
                                hintText: "Enter Your name"))),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 8),
                        child: TextField(
                          controller: surnameController,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            hintText: "Enter Your Surname",
                          ),
                        )),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 8),
                        child: TextField(
                            controller: provinceController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              hintText: "Enter Your Province",
                            ))),
                    TextButton(
                      style: ElevatedButton.styleFrom(
                        // Foreground color
                        onPrimary:
                            Theme.of(context).colorScheme.onSecondaryContainer,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 20),

                        // Background color
                        primary:
                            Theme.of(context).colorScheme.secondaryContainer,
                      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
                      onPressed: () {
                        _addSessions();
                      },
                      child: const Text(
                        'Register for Promo',
                      ),
                    )
                  ],
                ),
                const SessionsList()
              ],
            ),
          ),
        ));
  }
}
