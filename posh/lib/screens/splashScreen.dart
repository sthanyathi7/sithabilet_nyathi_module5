import 'dart:async';
import 'package:flutter/material.dart';
import './screen_index.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  @override
  void initState() {
    ;
    super.initState();
    Timer(
        const Duration(seconds: 5),
        () => Navigator.pushReplacement(context,
            MaterialPageRoute(builder: ((context) => const HomePage()))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("Splash Screen"),
      //   centerTitle: true,
      // ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image.asset(
              "assets/images/hair-logo.png",
              height: 260,
            ),

            // CircleAvatar(
            //   radius: (100),
            //   backgroundImage: AssetImage(
            //     'images/doll.png',
            //   ),
            // ),
            const SizedBox(
              height: 80,
              width: 80,
            ),
            const Image(
              image: AssetImage(
                'images/logo.png',
              ),
              height: 45,
            ),
            const SizedBox(
              height: 20,
              width: 20,
              child: CircularProgressIndicator(),
            ),
          ],
        ),
      ),
    );
  }
}
